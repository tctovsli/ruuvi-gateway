# A simple Python2 ruuvitag gateway

This is a simple script for receiving POST-request from the [Ruuvi Station Mobile application](https://f.ruuvi.com/t/tool-ruuvi-station-mobile-application/450) and putting the data into InfluxDB.

The script is written in Python 2 with flask.