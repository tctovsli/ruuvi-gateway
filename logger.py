#!ruuvi/bin/python
from flask import Flask, jsonify
from flask import request
from influxdb import InfluxDBClient


# Initialize influxdb backend
db = InfluxDBClient(host='localhost', port=8086, username='ruuvitagrw', password='RuuviPass!')
db.switch_database('ruuvitag')

app = Flask(__name__)
#@app.route('/api/v1/ruuvi', methods=['GET'])
#def get_measurements():
#    return jsonify({'measurements': measurements})

@app.route('/api/v1/ruuvi', methods=['POST'])
def log_measurement():
    measurements = []
    if not request.json or not 'deviceId' in request.json:
        abort(400)
    for tag in request.json['tags']:
        measurement = [
            {
                "measurement": "ruuvitag",
                "tags": {
                    "tagID": tag['id'],
                    "name": tag['name'],
                    "deviceID": request.json['deviceId']
                },
                "fields": {
                    "humidity": tag['humidity'],
                    "pressure": tag['pressure'],
                    "temperature": tag['temperature'],
                    "accelX": tag['accelX'],
                    "accelY": tag['accelY'],
                    "accelZ": tag['accelZ']
                }
            }
        ]
        print measurement
        measurements.append(measurement)
        try:
            db.write_points(measurement)
        except TypeError as e:
            print e
    return jsonify({'measurement': measurements}), 201

if __name__ == '__main__':
    app.run(debug=True)
